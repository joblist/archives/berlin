# joblist.city

This is a website (frontend) for [joblistberlin](https://joblistberlin.com).

## Overview

- on the website [joblistberlin.com](https://joblistberlin.com), search jobs, specifically in Berlin, using data from the joblist.city systems.
- on the website [joblist.city](https://joblist.city), it is possible to search (algolia) an index of currently open jobs (from a list of companies currently hirring).

## Deployment

- all the code is in `/index` 
- all of it is moved to `/public` when deployed, which it is served by gitlab pages (index.html)
- deployed with `.gitlab-ci.yml`, `pages` job.


## Setup

This website has had many forms and structure, migrating from txt
document, to spreadsheet, to ember, to react, to static site, mono to
multi repo, npm packages, algolia, gitlab workers, vercel functions.

Will probably continue to evolve, keeping this repo for CI and history?
